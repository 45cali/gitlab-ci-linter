module gitlab.com/45cali/gitlab-ci-linter

require (
	github.com/fatih/color v1.7.0
	github.com/go-ini/ini v1.41.0
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/urfave/cli v1.20.0
)
